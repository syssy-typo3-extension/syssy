<?php

namespace Syssy\Syssy\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use Syssy\Syssy\Library\Firebase\JWT\JWT;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Package\Exception as PackageException;

class SyssyController extends ActionController
{
	/**
	 * HTTP Header: Syssy-Api-Token
	 */
	public function infoAction(): ResponseInterface
	{

        $headers = getallheaders();

        if(isset($headers['Syssy-Api-Token'])){

            $apikey = $headers['Syssy-Api-Token'];
			
			//decode JWT token
			$APIkeydecoded = JWT::decode($apikey, JWT::urlsafeB64Encode($this->settings['apikey']), array('HS256'));

			if($APIkeydecoded == $this->settings['apikey']){
				
                $dbVersion = "";
                $pluginList = "";
				$phpVersion = phpversion();
				$mysqlVersion = "";
				$siteurl = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_SITE_URL');
				$homeurl = "";
				$pagetitle = $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'];
				$hostname = gethostname();

                $pluginList = $this->getExtensionList(\TYPO3\CMS\Core\Core\Environment::getPublicPath() . '/typo3conf/' . 'ext/');
				
				$versionInformation = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Information\Typo3Version::class);
				
				$infos['site-url'] = $siteurl;
				$infos['site-title'] = $pagetitle;
				$infos['cms-version'] = $versionInformation->getVersion();
				$infos['php-version'] = $phpVersion;
				$infos['server-software'] = $_SERVER['SERVER_SOFTWARE'];
				$infos['http-version'] = $_SERVER['SERVER_PROTOCOL'];
				$infos['server-ip'] = $_SERVER['SERVER_ADDR'];
				$infos['server-port'] = $_SERVER['SERVER_PORT'];
				$infos['hostname'] = $hostname;

                $db = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class);
                $dbConnection = $db->getConnectionByName(\TYPO3\CMS\Core\Database\ConnectionPool::DEFAULT_CONNECTION_NAME);
                $dbVersion = $dbConnection->getServerVersion();
				
				$infos['db-version'] = $dbVersion;
				$infos['plugininfo'] = $pluginList;
								
				$json = json_encode($infos);

				$urlsafeapikey = JWT::urlsafeB64Encode($APIkeydecoded);
				
				//create JWT
				$jwt = JWT::encode($json, $urlsafeapikey, 'HS256');
				
				return $this->responseFactory
				->createResponse()
				->withHeader('Content-Type', 'text/plain')
				->withBody($this->streamFactory->createStream($jwt));
				
			}
			else{
				die();
			}
		
		}
		else{
			
			die();
		}
		

	}
	
	/**
     * reads als extension in typo3conf/ext/
     *
     * @return array - return an array with all extensions
     */
    public static function getExtensionList($path)
    {
		
		//get loaded extension list
		$loadedExtensions = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getLoadedExtensionListArray();
		
		$pluginList = array();
		
		//read all loaded extensions
		foreach($loadedExtensions as $extKey){
			
			$version = "";
			$active = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded($extKey);
			
			try{
				$version = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getExtensionVersion($extKey);
			} catch(PackageException $e){
				
			}
			
			$pluginList[$extKey]['identifier'] = $extKey;
			$pluginList[$extKey]['name'] = $extKey;
			$pluginList[$extKey]['version'] = $version;
			$pluginList[$extKey]['active'] = $active;
			
			$extPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($extKey);
			
			if (@is_file($extPath . '/ext_emconf.php')) {
				$EM_CONF = array();

				$_EXTKEY = $extKey;

				//get extension info via EXT_CONF in ext_emconf.php
				include($extPath . '/ext_emconf.php');

				$extensioninfo[$extKey] = $EM_CONF;

				foreach ($extensioninfo[$extKey] as $info => $values) {

					foreach ($values as $key => $value) {
						
						if ($key == "title") {
							$pluginList[$extKey]['title'] = $value;
						}
						if ($key == "version") {
							$pluginList[$extKey]['version'] = $value;
						}
						if ($key == "state") {
							$pluginList[$extKey]['state'] = $value;
						}
					}
				}
			}
			
		}
		
        $list = array();
        if (@is_dir($path)) {
            $extList = \TYPO3\CMS\Core\Utility\GeneralUtility::get_dirs($path);
            if (is_array($extList)) {
                foreach ($extList as $extKey) {
                    if (@is_file($path . $extKey . '/ext_emconf.php')) {

						if(!isset($pluginList[$extKey])){
							$pluginName = $extKey;
							
							$pluginList[$pluginName]['name'] = $extKey;
							$pluginList[$pluginName]['title'] = '';
							$pluginList[$pluginName]['version'] = '';
							$pluginList[$pluginName]['state'] = '';
							
							if(in_array($pluginName, $loadedExtensions)){
								$pluginList[$pluginName]['active'] = 1;
							}
							else{
								$pluginList[$pluginName]['active'] = 0;
							}

							if(file_exists($path . $extKey . '/ext_emconf.php')) {

								$EM_CONF = array();

								$_EXTKEY = $extKey;

								//get extension info via EXT_CONF in ext_emconf.php
								include($path . $extKey . '/ext_emconf.php');

								$extensioninfo[$extKey] = $EM_CONF;

								foreach ($extensioninfo[$extKey] as $info => $values) {

									foreach ($values as $key => $value) {

										if ($key == "title") {
											$pluginList[$pluginName]['title'] = $value;
										}
										if ($key == "version") {
											$pluginList[$pluginName]['version'] = $value;
										}
										if ($key == "state") {
											$pluginList[$pluginName]['state'] = $value;
										}
									}
								}
							}
							else{
								$pluginList[$pluginName]['title'] = $extKey;
								$pluginList[$pluginName]['version'] = '';
								$pluginList[$pluginName]['state'] = '';
							}
						}
					
                    }
                }
            }
        }
        return $pluginList;
    }

}