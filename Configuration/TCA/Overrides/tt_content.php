<?php

use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

// Prevent script from being called directly
defined('TYPO3') or die();

(static function() {
	ExtensionUtility::registerPlugin(
		'Syssy',
		'Pi1',
		'Syssy'
	);
})();