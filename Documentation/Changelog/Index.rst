.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _changelog:

ChangeLog
=========

All changes are documented on `https://gitlab.com/syssy-typo3-extension/syssy <https://gitlab.com/syssy-typo3-extension/syssy>`_.
Please follow the link to know which bugs have been fixed in which version.


.. _list_of_versions:

List of versions
----------------
* 3.0.3: 20th Ocotber 2023 - Rename files, update documentation
* 3.0.2: 6th September 2023 - Bugfix
* 3.0.1: 6th September 2023 - Bugfix extension list for composer installations
* 3.0.0: 4th September 2023 - Support for TYPO3 12
* 2.0.5: 26th June 2023 - Add additional project info "hostname"
* 2.0.4: 12th May 2023 - Bugfix for TYPO3 11 and PHP 8
* 2.0.3: 23rd February 2023 - Bugfix for TYPO3 11, Remove deprecated "TYPO3_version"
* 2.0.2: 15th August 2022 - Update documentation and set state to stable
* 2.0.1: 19th January 2022 - Update composer name
* 2.0.0: 17th January 2022 - Support TYPO3 10 and 11, skip support for TYPO3 7, 8 and 9
* 1.9.1: 6th December 2021 - Update documentation
* 1.9.0: 5th December 2021 - Publish version to TER
