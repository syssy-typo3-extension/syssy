.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _configuration:

Configuration
=============

Target group: **Administrators**

.. rst-class:: bignums

   1. Get API key from SYSSY
   
      You can find your API key on `app.syssy.net <app.syssy.net>`_ in your project dashboard

   2. Include Static Template "SYSSY"
   
      Go to :guilabel:`Template` > :guilabel:`Root Page (Home)` > :guilabel:`Info/Modify`

      Select :guilabel:`Edit the whole template record`
 
      Select tab :guilabel:`Includes` and add SYSSY to selected items
   
   3. Add TypoScript configuration (constants) for SYSSY API key
   
      Go to :guilabel:`Template` > :guilabel:`Your Home Page` > :guilabel:`Constant Editor` and select :guilabel:`PLUGIN.TX_SYSSY` and enter your API key in field :guilabel:`API-key for syssy.net` and go to :guilabel:`Save`

      Alternatively you can add the TypoScript to your Constants:
	  
      .. code-block:: bash
	  
         plugin.tx_syssy.settings.apikey = 83dedc900894xxxxxxxxxxxxxxxxx

   4. Check if connection is working
      
      Go to `app.syssy.net <app.syssy.net>`_ and select your project. Start :guilabel:`Remonitor` for the project in project list or project dashboard or run :guilabel:`Test API` on project dashboard.
	  
	  
      SYSSY project dashboard:
	  
      .. figure:: /Images/syssy_remonitoring_en.png
         :alt: Screenshot of remonitoring
		 
	
	
      Run API test in SYSSY:
	  
      .. figure:: /Images/syssy_api_test_en.png
         :alt: Screenshot of API Test

