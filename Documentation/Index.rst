.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=======================================================
SYSSY: extension for connecting TYPO3 with app.syssy.net
=======================================================

.. only:: html

    :Classification:
        syssy

    :Version:
        |release|

    :Language:
        en

    :Description:
        SYSSY is an online tool for managing and monitoring your websites. With this extension you can connect the SYSSY backend with your extension and get information about your TYPO3 system and server. Keep track of all your projects and quickly identify issues. Start right now for free and see for yourself. => `syssy.net <http://www.syssy.net/>`_

    :Keywords:
        syssy, managing websites, monitoring websites, website manager

    :Copyright:
        2020-2022

    :Author:
        SYSSY Online GmbH

    :Email:
        office@syssy.net

    :License:
        This document is published under the `Creative Commons BY 4.0 <https://creativecommons.org/licenses/by/4.0/>`__ license.

    :Rendered:
        |today|

   	
    .. seealso::
       `Test for free <https://www.syssy.net/kostenlos-testen>`__


    **Table of Contents**

.. toctree::
    :maxdepth: 5
    :titlesonly:
    :glob:

    Introduction/Index
    Installation/Index
    Configuration/Index
    Support/Index
    Changelog/Index
