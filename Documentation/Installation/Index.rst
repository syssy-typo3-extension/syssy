.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _administration:

Installation
=====================

Target group: **Administrators**

.. _installation_composer:

Installation via Composer
------------

If your TYPO3 installation uses Composer, install the latest Extension Builder through:

.. code-block:: bash

   composer require syssy/syssy-typo3-extension


.. _installation_legacy:

Installation without composer / Legacy mode
------------

If you are working with a TYPO3 installation that does not use Composer, install the extension in the Extension Manager:

#. Go to the Extension Manager
#. Install the extension SYSSY
#. Include the static template SYSSY

