.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _introduction:

Introduction
============

.. _what-does-it-do:

What does it do?
----------------

The SYSSY extensions is a way to connect your TYPO3 installation with SYSSY platform.

SYSSY is an online platform for managing and monitoring websites.
We provide plugins to connect different CMS (content management systems) with SYSSY, so we can receive data about your websites and inform you if there is any problem or if any security updates are necessary in your system or on your server.
On SYSSY platform you can see a list of all your projects and if there is any need for a security update in your websites.

You can connect your TYPO3 website and SYSSY to this TYPO3 extension.
Just get your project API key from `app.syssy.net <https://app.syssy.net>`_ and connect your website with SYSSY.
SYSSY calls your TYPO3 website via a specific page type and fetches your TYPO3 version, a list of installed extensions and their versions and PHP and MySQL version.
SYSSY is always up to date and generates a great overview of all your website projects.

Find more information on `www.syssy.net <https://www.syssy.net>`_.

Find our general terms and conditions here: `Terms and conditions <https://www.syssy.net/en/terms-and-conditions>`_ and our terms of service here: `Terms of service <https://www.syssy.net/en/terms-of-service>`_

Features of SYSSY
-----------------

* Monitor all your websites with just one platform
* Permanent HTTP monitoring of your websites and emails or app notifications if problems occur
* Regularly CMS monitoring of the CMS (TYPO3) - get informed when an update needs to be done
* Get a list of your extensions and versions
* Check PHP and MySQL version via SYSSY and get informed when an update needs to be done
* Performance monitoring with Google Lighthouse including a performance history
* History and PDF export of your data
* SEO check of your landing page
* GDPR check for your landing page
* Customer management

.. seealso::
   `Test for free <https://www.syssy.net/kostenlos-testen>`__

Why SYSSY?
-----------------
SYSSY is not just another monitoring tool. SYSSY works with multiple CMS, not only TYPO3. There are plugins for WordPress, Contao, Drupal, Joomla!, PrestaShop and Magento.
Out target is to give you an overview of all your web projects and import information like the security state, SEO information and performance information.
A clearly arranged table shows all your projects and their status at a glance.

Screenshots
-----------------
List of websites in SYSSY backend:

.. figure:: /Images/monitoring_desktop_en.png
   :alt: Screenshot of monitoring list
 

Project detail view:

.. figure:: /Images/project_performance_en_desktop.png
   :alt: Screenshot of project detail view


Plugin list for a single project:

.. figure:: /Images/project_plugins_de_tablet.png
   :alt: Screenshot of project plugin list


SEO information for a single project:

.. figure:: /Images/project_seo_en_detail.png
   :alt: Screenshot of project SEO information