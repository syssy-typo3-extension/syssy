.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _support:

FAQ/Support
===========


Frequently Asked Questions
--------------------------

How can I test if the connection works?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Login to `app.syssy.net <https://app.syssy.net>`_ and go to your project's dashboard.

Click the :guilabel:`remonitoring button` and check if it works.

You can also run the :guilabel:`Test API` button.

If it's not working, please recheck your API-key and check in your TYPO3 backend if the key is set.


Bug reports and feature requests
--------------------------------

Found a problem with this extension?

If you find any problems in this extension or the manual, please send us a mail to support@syssy.net.

If you have any feedback or feature request, please send us a mail to feedback@syssy.net.