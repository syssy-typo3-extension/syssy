# SYSSY TYPO3 Extension

Extension for connecting TYPO3 with SYSSY platform.

SYSSY is an online platform for managing and monitoring websites.
We provide plugins to connect different CMS (content management systems) with SYSSY, so we can receive data about your website and inform you if there is any problem or if any security updates are necessary in your system or on your server.
On SYSSY platform you can see a list of all your projects and if there is any need for a security update in your websites.

You can connect your TYPO3 website and SYSSY to this TYPO3 extension.
Just get your project API key from [app.syssy.net](https://app.syssy.net) and connect your website with SYSSY.
SYSSY calls your TYPO3 website via a specific page type and fetches your TYPO3 version, a list of installed extensions and their versions and PHP and MySQL version.

Find more information on [www.syssy.net](https://www.syssy.net).
Please find our general terms and conditions here: [Terms and conditions](https://www.syssy.net/en/terms-and-conditions)
Please find our terms of service here: [Terms of service](https://www.syssy.net/en/terms-of-service)

## Requirements

This extension works with 
- TYPO3 11
- TYPO3 12

Older versions of this extension work with
- TYPO3 7
- TYPO3 8
- TYPO3 9
- TYPO3 10


## Installation & Configuration

Find a full documentation in the [TYPO3 documentation of the plugin](https://docs.typo3.org/p/syssy/syssy-typo3-extension/main/en-us/)


## Feedback

Found a problem with this extension?
If you find any problems in this extension or the manual, please send us a mail to support@syssy.net.

If you have any feedback or feature request, please send us a mail to feedback@syssy.net.
