<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'SYSSY - Website Monitor',
    'description' => 'Connects your website to SYSSY.net to monitor your website. Get informed about website outages and security issues in TYPO3 and extensions.',
    'category' => 'plugin',
    'author' => 'Ingrid Stürmer',
    'author_company' => 'SYSSY Online GmbH',
    'author_email' => 'office@syssy.net',
    'state' => 'stable',
    'clearCacheOnLoad' => true,
    'version' => '3.0.4',
    'constraints' => [
        'depends' => [
            'typo3' => '11.0.0-13.9.99',
        ],
    ],
];