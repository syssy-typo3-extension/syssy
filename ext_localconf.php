<?php
defined('TYPO3') or die();

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Syssy',
    'Pi1',
    [
        \Syssy\Syssy\Controller\SyssyController::class => 'info',
    ],
    // non-cacheable actions
    [
        \Syssy\Syssy\Controller\SyssyController::class => 'info',
    ]
);
